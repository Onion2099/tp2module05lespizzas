﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BO
{
    public class Pizza
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Le champ Nom doit être compris entre 5 et 20 caractères")]
        [MinLength(5), MaxLength(20)]
        public string Nom { get; set; }
        
        public Pate Pate { get; set; }

        public List<Ingredient> Ingredients { get; set; } = new List<Ingredient>();

    }
}
