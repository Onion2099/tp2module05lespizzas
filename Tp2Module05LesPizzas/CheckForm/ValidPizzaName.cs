﻿using BO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Tp2Module05LesPizzas.Utils;

namespace Tp2Module05LesPizzas.CheckForm
{
    public class ValidPizzaName : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            Pizza pizza = value as Pizza;

            if (FakeDbPizza.Instance.Pizzas.Any(p => p.Nom.ToUpper() == pizza.Nom.ToUpper() && p.Id != pizza.Id))
            {
                return false;
            }
            else
            {
                return true;
            }

        }
    }
}