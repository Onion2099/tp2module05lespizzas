﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BO;
using Tp2Module05LesPizzas.Models;
using Tp2Module05LesPizzas.Utils;
using System.ComponentModel.DataAnnotations;

namespace Tp2Module05LesPizzas.Controllers
{
    public class PizzaController : Controller
    {
        // GET: Pizza
        public ActionResult Index()
        {
            return View(FakeDbPizza.Instance.Pizzas);
        }

        // GET: Pizza/Details/id
        public ActionResult Details(int id)
        {
            Pizza pizza = FakeDbPizza.Instance.Pizzas.FirstOrDefault(p => p.Id == id);
            return View(pizza);
        }

        // GET: Pizza/Create
        public ActionResult Create()
        {
            PizzaGestionVM pizzaGestionVM = new PizzaGestionVM()
            { 
                Pizza = new Pizza(), 
                Ingredients = FakeDbPizza.Instance.IngredientsDisponibles, 
                Pates = FakeDbPizza.Instance.PatesDisponibles 
            };
            return View(pizzaGestionVM);
        }

        // POST: Pizza/Create
        [HttpPost]
        public ActionResult Create(PizzaGestionVM pizzaGestionVM)
        {
            try
            {
                if (ModelState.IsValid && IsNameUnique(pizzaGestionVM) && IsIngredientListUnique(pizzaGestionVM))
                {
                    Pizza pizza = new Pizza();
                    pizza.Id = FakeDbPizza.Instance.Pizzas.Count == 0 ? 1 : FakeDbPizza.Instance.Pizzas.Max(x => x.Id) + 1;
                    pizza.Nom = pizzaGestionVM.Pizza.Nom;
                    pizza.Pate = FakeDbPizza.Instance.PatesDisponibles.FirstOrDefault(p => p.Id == pizzaGestionVM.IdPate);
                    pizza.Ingredients = FakeDbPizza.Instance.IngredientsDisponibles.Where(i => pizzaGestionVM.IdIngredients.Contains(i.Id)).ToList();
                    FakeDbPizza.Instance.Pizzas.Add(pizza);
                    return RedirectToAction("Index");
                }
                pizzaGestionVM.Ingredients = FakeDbPizza.Instance.IngredientsDisponibles;
                pizzaGestionVM.Pates = FakeDbPizza.Instance.PatesDisponibles;
                return View(pizzaGestionVM);
            }
            catch
            {
                pizzaGestionVM.Ingredients = FakeDbPizza.Instance.IngredientsDisponibles;
                pizzaGestionVM.Pates = FakeDbPizza.Instance.PatesDisponibles;
                return View(pizzaGestionVM);
            }
        }

        // GET: Pizza/Edit/id
        public ActionResult Edit(int id)
        {
            Pizza pizza = FakeDbPizza.Instance.Pizzas.FirstOrDefault(p => p.Id == id);
            List<int> idIngredients = new List<int>();

            foreach (Ingredient ingredient in pizza.Ingredients)
            {
                idIngredients.Add(ingredient.Id);
            }

            PizzaGestionVM pizzaGestionVM = new PizzaGestionVM()
            {
                Pizza = pizza,
                IdPate = pizza.Pate.Id,
                IdIngredients = idIngredients,
                Ingredients = FakeDbPizza.Instance.IngredientsDisponibles,
                Pates = FakeDbPizza.Instance.PatesDisponibles
            };

            return View(pizzaGestionVM);
        }

        // POST: Pizza/Edit/id
        [HttpPost]
        public ActionResult Edit(int id, PizzaGestionVM pizzaGestionVM)
        {
            try
            {
                if (ModelState.IsValid && IsNameUnique(pizzaGestionVM) && IsIngredientListUnique(pizzaGestionVM))
                {
                    Pizza pizza = FakeDbPizza.Instance.Pizzas
                        .FirstOrDefault(p => p.Id == id);
                    
                    pizza.Nom = pizzaGestionVM.Pizza.Nom;
                    
                    pizza.Ingredients = FakeDbPizza.Instance.IngredientsDisponibles
                        .Where(i => pizzaGestionVM.IdIngredients
                        .Contains(i.Id))
                        .ToList();
                    
                    pizza.Pate = FakeDbPizza.Instance.PatesDisponibles
                        .FirstOrDefault(p => p.Id == pizzaGestionVM.IdPate);
                   
                    return RedirectToAction("Index");
                }
                pizzaGestionVM.Ingredients = FakeDbPizza.Instance.IngredientsDisponibles;
                pizzaGestionVM.Pates = FakeDbPizza.Instance.PatesDisponibles;
                return View(pizzaGestionVM);

            }
            catch
            {
                pizzaGestionVM.Ingredients = FakeDbPizza.Instance.IngredientsDisponibles;
                pizzaGestionVM.Pates = FakeDbPizza.Instance.PatesDisponibles;
                return View(pizzaGestionVM);
            }
        }

        // GET: Pizza/Delete/id
        public ActionResult Delete(int id)
        {
            PizzaGestionVM pizzaGestion = new PizzaGestionVM();
            pizzaGestion.Pizza = FakeDbPizza.Instance.Pizzas.FirstOrDefault(x => x.Id == id);
            return View(pizzaGestion);
        }

        // POST: Pizza/Delete/id
        [HttpPost]
        public ActionResult Delete(int id, Pizza pizza)
        {
            try
            {
                FakeDbPizza.Instance.Pizzas.Remove(FakeDbPizza.Instance.Pizzas.FirstOrDefault(p => p.Id == id));
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        private bool ValidateVM(PizzaGestionVM pizzaGestionVM)
        {
            bool result = true;
            return result;
        }

        private bool IsNameUnique(PizzaGestionVM pizzaGestionVM)
        {
            bool result = !FakeDbPizza.Instance.Pizzas.Any(p => p.Nom == pizzaGestionVM.Pizza.Nom);
            if (!result)
                ModelState.AddModelError("Pizza.Nom", "Il existe déjà une pizza portant ce nom");
            return result;
        }

        private bool IsIngredientListUnique(PizzaGestionVM pizzaGestionVM)
        {
            bool result = true;

            foreach (var pizza in FakeDbPizza.Instance.Pizzas)
            {
                if (pizza.Ingredients.Select(i => i.Id).SequenceEqual(pizzaGestionVM.IdIngredients))
                {
                    result = false;
                    ModelState.AddModelError("IdIngredients", "Il existe déjà une pizza composée de ces ingrédients");
                    break;
                }
            }
            return result;
        }

    }
}
