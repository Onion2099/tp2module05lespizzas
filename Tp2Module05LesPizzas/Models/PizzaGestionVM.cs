﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BO;
using Tp2Module05LesPizzas.CheckForm;

namespace Tp2Module05LesPizzas.Models
{
    public class PizzaGestionVM
    {
        public Pizza Pizza { get; set; }

        [Required]
        [DisplayName("Pates")]
        public int IdPate { get; set; }
        public List<Pate> Pates { get; set; }

        [Required]
        [DisplayName("Ingrédients")]
        public List<int> IdIngredients { get; set; }
        public List<Ingredient> Ingredients { get; set; }

    }
}