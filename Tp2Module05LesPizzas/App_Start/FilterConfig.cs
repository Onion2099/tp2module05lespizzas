﻿using System.Web;
using System.Web.Mvc;

namespace Tp2Module05LesPizzas
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
